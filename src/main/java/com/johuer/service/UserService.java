package com.johuer.service;

import com.johuer.dao.UserDao;
import com.johuer.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author Administrator
 * @create 2016-09-26-11:52
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public int insert(User user) {
        return userDao.insert(user);
    }

    public int insertBatch(List<User> list) {
        return userDao.insertBatch(list);
    }
}
