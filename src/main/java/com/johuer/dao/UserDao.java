package com.johuer.dao;

import com.johuer.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author xujiuhua
 * @create 2016-09-26-11:52
 */
@Repository
public interface UserDao {

    int insert(User user);

    int insertBatch(List<User> list);

}
