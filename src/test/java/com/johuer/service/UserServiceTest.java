package com.johuer.service;

import com.johuer.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static junit.framework.TestCase.assertEquals;

/**
 * ${DESCRIPTION}
 *
 * @author xujiuhua
 * @create 2016-09-26-13:41
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class UserServiceTest {

    @Autowired
    private UserService userService;

    private List<User> list = new ArrayList<User>();
    private int INSERT_COUNT = 1000;

    @Before
    public void before() throws Exception {
        for(int i=0; i<INSERT_COUNT; i++) {
            User user = new User();
            user.setUserName(UUID.randomUUID().toString().replace("-", "").substring(0, 30));
            user.setPassword("123456");
            list.add(user);
        }
    }

    @After
    public void after() throws Exception {
    }

    /**
     *
     * Method: insert(User user)
     *
     */
    @Test
    public void testInsert() throws Exception {
        Long start = System.currentTimeMillis();
        for (User user : list) {
           userService.insert(user);
        }
        Long end = System.currentTimeMillis();
        System.out.println("testInsert-->Time:" + (end-start));
        assertEquals(INSERT_COUNT, INSERT_COUNT);
    }

    /**
     *
     * Method: insertBatch(List<User> list)
     *
     */
    @Test
    public void testInsertBatch() throws Exception {
        Long start = System.currentTimeMillis();
        int resultCount = userService.insertBatch(list);
        Long end = System.currentTimeMillis();
        System.out.println("testInsertBatch-->Time:" + (end-start));
        assertEquals(INSERT_COUNT, resultCount);
    }
}
