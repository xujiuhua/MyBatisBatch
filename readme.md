### Mybatis 批量处理数据

> 适用于1000条数据以上，当然1000条数据以下没必要使用`insertBatch`

```xml
<!--外部循环n次-->
<insert id="insert" parameterType="com.johuer.entity.User">
    INSERT INTO
    t_user (user_name, credits, password, last_visit,last_ip)
    VALUES (#{userName}, #{credits}, #{password}, #{lastVisit}, #{lastIp})
</insert>

<!--foreach-->
<insert id="insertBatch">
    INSERT INTO
    t_user (user_name, credits, password, last_visit,last_ip)
    VALUES
    <foreach collection="list" item="item" index="index" separator=",">
        (#{item.userName},#{item.credits},#{item.password},#{item.lastVisit},#{item.lastIp})
    </foreach>
</insert>
```